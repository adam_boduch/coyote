<?php
/**
 * @package Coyote-F
 * @author Adam Boduch <adam@boduch.net>
 * @copyright Copyright (c) Adam Boduch (Coyote Group)
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

/**
 * Walidator sprawdza, czy dana wartosc znajduje sie w tablicy
 * @todo Zmiana nawy tego pliku na inArray.class.php
 */
class Validate_InArray extends Validate_Abstract implements IValidate
{
	const NOT_IN_ARRAY						= 1;
	
	protected $templates = array(
			self::NOT_IN_ARRAY				=> 'Wartość "%value%" nie znajduje się w zbiorze danych'
	);

	protected $array;
	protected $caseSensitive;
	
	function __construct(array $array = array(), $caseSensitive = false)
	{
		if ($array)
		{
			$this->setArray($array);
		}

		$this->setCaseSensitive($caseSensitive);
	}
	
	public function setArray(array $array)
	{
		$this->array = $array;
		return $this;
	}
	
	public function getArray()
	{
		return $this->array;
	}

	public function setCaseSensitive($flag)
	{
		$this->caseSensitive = (bool) $flag;
		return $this;
	}

	public function isCaseSensitive()
	{
		return $this->caseSensitive;
	}
	
	/**
	 * Walidacja
	 * @param $value
	 * @return bool
	 */
	public function isValid($value)
	{
		$this->setValue($value);

		if ($this->isCaseSensitive() === false)
		{
			$value = Text::toLower($value);
			$this->array = array_map(array('Text', 'toLower'), $this->array);
		}
		
		if (in_array($value, $this->array))
		{
			return true;
		}
		else
		{
			$this->setError(self::NOT_IN_ARRAY);
		}
		
		return ! $this->hasErrors();		
	}
	
}
?>